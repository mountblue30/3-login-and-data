const fs = require("fs/promises");
const path = require("path");
const fsCallback = require("fs")


function createAFile(fileName, data = "Dummy data!") {
    return fs.writeFile(path.join(__dirname, fileName), data)
        .then(() => {
            console.log(`File created - ${fileName}`);
        })
        .catch((err) => {
            console.log(err);
        })
}
function appendIntoAFile(fileName, data = "Dummy data!") {
    return fs.appendFile(path.join(__dirname, fileName), data)
        .catch((err) => {
            console.log(err);
        })
}

function readAFile(fileName) {
    return fs.readFile(path.join(__dirname, fileName))
        .then((data) => {
            console.log(`File read successful - ${fileName}`);
            return data.toString();
        })
        .catch((err) => {
            console.log(err);
        })
}
function deleteAFile(fileName) {
    return fs.unlink(path.join(__dirname, fileName))
        .then(() => {
            console.log(`File deleted - ${fileName}`);
        })
        .catch((err) => {
            console.log(err);
        })
}

function createAFileCallback(fileName, data = "Dummy data!", callback) {
    return fsCallback.writeFile(path.join(__dirname, fileName), data, callback)

}

function readAFileCallback(fileName, callback) {
    return fsCallback.readFile(path.join(__dirname, fileName), callback);

}

function deleteAFileCallback(fileName, callback) {
    return fsCallback.unlink(path.join(__dirname, fileName), callback);

}
function createAndDeleteFiles() {
    let files = ["1.txt", "2.txt"];
    Promise.allSettled(files.map((file) => {
        return createAFile(file);
    }))
        .then(() => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    return resolve();
                }, 2000);
            })
        })
        .then(() => {
            return Promise.allSettled(files.map((file) => {
                return deleteAFile(file);
            }))
        })

        .catch((err) => {
            console.log(err);
        })
}

// createAndDeleteFiles()


function lipsumDataUsingPromises() {
    fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1")
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw new Error(`${data.status} - ${data.statusText}`);
            }
        })
        .then((data) => {
            return createAFile("lipsum.txt", data[0]);
        })
        .then(() => {
            return readAFile("lipsum.txt");
        })
        .then((data) => {
            return createAFile("lipsumNewFile.txt", data[0]);
        })
        .then(() => {
            return deleteAFile("lipsum.txt");
        })
        .catch((err) => {
            console.log(err);
        })

}

// lipsumDataUsingPromises()

function lipsumDataUsingCallbacks() {
    return fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1")
        .then((data) => {
            if (data.ok) {
                return data.json();
            }
        })
        .then((data) => {
            createAFileCallback("lipsum.txt", data[0], (err, data) => {
                if (err) {
                    throw new Error(err);
                } else {
                    console.log(`File created - lipsum.txt .`);
                    readAFileCallback("lipsum.txt", (err, data) => {
                        if (err) {
                            throw new Error(err);
                        } else {
                            console.log(`File read successfully - lipsum.txt.`);
                            createAFileCallback("lipsumNewFile.txt", data.toString(), (err, data) => {
                                if (err) {
                                    throw new Error(err);
                                } else {
                                    console.log("Created new file with lipsum data - lipsumNewFile.txt");
                                    deleteAFileCallback("lipsum.txt", (err) => {
                                        if (err) {
                                            throw new Error(err);
                                        } else {
                                            console.log("The lipsum.txt is deleted.");
                                        }
                                    })

                                }
                            })
                        }
                    })
                }
            })
        })
}

// lipsumDataUsingCallbacks()




function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        },
        {
            id: 3,
            name: "Test 3",
        }
    ]);
}

function logData(user, activity) {
    let userData = "";
    if (activity.includes("Success")) {
        userData += `\n ${activity} - ${JSON.stringify(user)}. \n ${activity} time - ${new Date()} \n`
    } else {
        userData += `\n ${activity} - ${user}. \n ${activity} time - ${new Date()} \n Reason - ${activity}\n`
    }    
    return appendIntoAFile(`${activity}.log`, userData)
    .then(()=>{
        return Promise.resolve(activity);
    })
    .catch((err)=>{
        return Promise.reject(activity);

    })
}

function getUserData(data, id) {
    let userData = data.filter((user) => {
        return user.id === id;
    });
    return userData;
}

function verifyLogin(user, id) {
    login(user, id)
        .then(() => {
            return getData();
        })
        .then((data) => {
            if (getUserData(data, id).length !== 1) {
                let userDetail = getUserData(data, id);
                return Promise.allSettled([logData(user, "loginSuccess"), logData("Data not present.", "getDataFailure")]);
            } else {
                return Promise.allSettled([logData(userDetail, "loginSuccess"), logData(userDetail, "getDataSuccess")]);
            }
        })
        .then((data) => {
            console.log(`Logs updated - ${data[0]["value"]}.log and ${data[1]["value"]}.log`);
        })
        .catch((err) => {
            getData()
                .then((data) => {
                    if (getUserData(data, id).length !== 1) {
                        return Promise.allSettled([logData(user, "loginFailure"), logData("Data not present.", "getDataFailure")]);
                    } else {
                        return Promise.allSettled([logData(user, "loginFailure"), logData(getUserData(data, id), "getDataSuccess")]);
                    }
                })
                .then((data) => {
                    console.log(`Logs updated - ${data[0]["value"]}.log and ${data[1]["value"]}.log`);                    
                })
                .catch((err) => {
                    console.log(err);
                })
        })
}

verifyLogin("Test 2", 2)